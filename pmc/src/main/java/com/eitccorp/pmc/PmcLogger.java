package com.eitccorp.pmc;

import java.io.IOException;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

/**
 * Logger for debugging
 * 
 * @author Jimmy Nguyen
 * @since 11/06/2019
 *
 */

public class PmcLogger {

	private static final Logger LOGGER = Logger.getLogger(PmcLogger.class.getName());
	private static FileHandler fileHandler;
	private static final String FILE_PATH = "pmc_logs.log";

	private PmcLogger() {};

	public static void logEvent(Level level, String msg) {
		try {
			LOGGER.setUseParentHandlers(false); // If we want to hide messages from terminal/console.
			fileHandler = new FileHandler(FILE_PATH, true);
			LOGGER.addHandler(fileHandler);
			fileHandler.setFormatter(new SimpleFormatter());
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			LOGGER.log(level, msg);
			fileHandler.close();
		}
	}

}
