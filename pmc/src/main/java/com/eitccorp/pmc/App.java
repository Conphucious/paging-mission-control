package com.eitccorp.pmc;

import java.io.File;
import java.util.Scanner;

import com.eitccorp.pmc.handler.TelemetryHandler;

/**
 * Enlighten Programming Challenge :: Paging Mission Control
 * 
 * Ingest status telemetry data and create alert messages for the following
 * violation conditions:
 * 
 * If for the same satellite there are three battery voltage readings that are
 * under the red low limit within a five minute interval. If for the same
 * satellite there are three thermostat readings that exceed the red high limit
 * within a five minute interval.
 * 
 * 
 * Input Format The program is to accept a file as input. The file is an ASCII
 * text file containing pipe delimited records. The ingest of status telemetry
 * data has the format:
 * <timestamp>|<satellite-id>|<red-high-limit>|<yellow-high-limit>|<yellow-low-limit>|<red-low-limit>|<raw-value>|<component>
 * You may assume that the input files are correctly formatted. Error handling
 * for invalid input files may be ommitted.
 * 
 * Output Format The output will specify alert messages. The alert messages
 * should be valid JSON with the following properties: { "satelliteId": 1234,
 * "severity": "severity", "component": "component", "timestamp": "timestamp" }
 * The program will output to screen or console (and not to a file).
 * 
 * Sample Data The following may be used as sample input and output datasets.
 * 
 * Input 20180101 23:01:05.001|1001|101|98|25|20|99.9|TSTAT 20180101
 * 23:01:09.521|1000|17|15|9|8|7.8|BATT 20180101
 * 23:01:26.011|1001|101|98|25|20|99.8|TSTAT 20180101
 * 23:01:38.001|1000|101|98|25|20|102.9|TSTAT 20180101
 * 23:01:49.021|1000|101|98|25|20|87.9|TSTAT 20180101
 * 23:02:09.014|1001|101|98|25|20|89.3|TSTAT 20180101
 * 23:02:10.021|1001|101|98|25|20|89.4|TSTAT 20180101
 * 23:02:11.302|1000|17|15|9|8|7.7|BATT 20180101
 * 23:03:03.008|1000|101|98|25|20|102.7|TSTAT 20180101
 * 23:03:05.009|1000|101|98|25|20|101.2|TSTAT 20180101
 * 23:04:06.017|1001|101|98|25|20|89.9|TSTAT 20180101
 * 23:04:11.531|1000|17|15|9|8|7.9|BATT 20180101
 * 23:05:05.021|1001|101|98|25|20|89.9|TSTAT 20180101
 * 23:05:07.421|1001|17|15|9|8|7.9|BATT
 * 
 * Ouput [ { "satelliteId": 1000, "severity": "RED HIGH", "component": "TSTAT",
 * "timestamp": "2018-01-01T23:01:38.001Z" }, { "satelliteId": 1000, "severity":
 * "RED LOW", "component": "BATT", "timestamp": "2018-01-01T23:01:09.521Z" } ]
 * 
 * @author Jimmy Nguyen
 * @since 11/06/2019
 *
 */
public class App {

	public static void main(String[] args) {
		// Can be changed to take CLI arg if need be.
		String input = "";
		Scanner scanner = new Scanner(System.in);
		while (true) {
			System.out.println("Enter a file path. (sample_input.txt for default test): ");
			input = scanner.nextLine();

			if (input.trim().toLowerCase().equals("exit"))
				break;
			if (!(new File(input).exists())) {
				System.out.println("Invalid file. try again");
			} else if (!input.equals("")) {
				TelemetryHandler th = new TelemetryHandler("sample_input.txt");
				th.checkStatus();
			}
		}
	}

}
