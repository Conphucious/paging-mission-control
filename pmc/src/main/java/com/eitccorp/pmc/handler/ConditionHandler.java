package com.eitccorp.pmc.handler;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.logging.Level;

import com.eitccorp.pmc.PmcLogger;
import com.eitccorp.pmc.model.Component;
import com.eitccorp.pmc.model.JsonData;
import com.eitccorp.pmc.model.SeverityType;
import com.eitccorp.pmc.model.TelemetryData;
import com.google.gson.GsonBuilder;

/**
 * Class that handles condition for same satellite out of bounds readings.
 * Checks for 3 red low limit battery voltage readings (in a 5 minute interval)
 * Checks for 3 red high limit thermostat readings (in a 5 minute interval)
 * 
 * @author Jimmy Nguyen
 * @since 11/06/2019
 *
 */
public class ConditionHandler {

	private HashMap<Integer, Integer> highMap; // Satellite ID is key, value is occurances within 5 minutes
	private HashMap<Integer, Integer> lowMap;
	private Queue<TelemetryData> telemetryQueue;
	private final int intervalInMs = 300000;
	private List<TelemetryData> highFlagged;
	private List<TelemetryData> lowFlagged;

	public ConditionHandler() {
		highMap = new HashMap<>();
		lowMap = new HashMap<>();
		telemetryQueue = new LinkedList<>();
		highFlagged = new ArrayList<>();
		lowFlagged = new ArrayList<>();
	}

	/**
	 * Starts the check for our warning conditions.
	 * 
	 * @param td
	 *            TelemetryData object.
	 * 
	 */
	public void checkCondition(TelemetryData td) {
		if (telemetryQueue.isEmpty()) {
			telemetryQueue.add(td);
			determineConstraints(td);
		} else {
			determineConstraints(td);
			telemetryQueue.add(td);
		}
	}

	/**
	 * Determines if the time condition and raw value is within constraints and
	 * handles it accordingly.
	 * 
	 * @param td
	 *            TelemetryData object.
	 */
	private void determineConstraints(TelemetryData td) {
		// make sure we are talking about same Satellite for time comparison
		TelemetryData previousData = getLastSatellite(td);

		boolean time = td.getDateTime().getMillis() - previousData.getDateTime().getMillis() <= intervalInMs;

		// Makes sure we are comparing to within the 5 minute time frame.
		// If time frame has expired then we reset hashmap containing our warnings.
		// If queue is empty then we have no outstanding warnings.
		if (!time) {
			if (td.getComponent() == Component.BATT) {
				highMap.put(td.getSatelliteId(), 0);
			} else if (td.getComponent() == Component.TSTAT) {
				lowMap.put(td.getSatelliteId(), 0);
			}

			while (true) {
				telemetryQueue.remove();
				previousData = getLastSatellite(td);
				if (td.getDateTime().getMillis() - previousData.getDateTime().getMillis() <= intervalInMs) {
					break;
				} else if (telemetryQueue.isEmpty()) {
					telemetryQueue.add(td);
					break;
				}
			}
		}

		if (td.getComponent() == Component.BATT && td.getRawValue() < td.getRedLowLimit()) {
			// Checks red low on BATT
			if (lowMap.get(td.getSatelliteId()) != null) {
				int currentOccurance = lowMap.get(td.getSatelliteId());
				lowMap.put(td.getSatelliteId(), ++currentOccurance);
				lowFlagged.add(td);
			} else {
				lowMap.put(td.getSatelliteId(), 1);
				lowFlagged.add(td);
			}

			PmcLogger.logEvent(Level.WARNING,
					"Satellite: " + lowMap.get(td.getSatelliteId()) + " - " + td.getComponent()
							+ " reading low red limits on " + td.getComponent() + " >> " + td.getTimestamp());
		} else if (td.getComponent() == Component.TSTAT && td.getRawValue() > td.getRedHighLimit()) {
			// Checks red high on TSTAT
			if (highMap.get(td.getSatelliteId()) != null) {
				int currentOccurance = highMap.get(td.getSatelliteId());
				highMap.put(td.getSatelliteId(), ++currentOccurance);
				highFlagged.add(td);
			} else {
				highMap.put(td.getSatelliteId(), 1);
				highFlagged.add(td);
			}

			PmcLogger.logEvent(Level.WARNING,
					"Satellite: " + highMap.get(td.getSatelliteId()) + " - " + td.getComponent()
							+ " reading high red limits on " + td.getComponent() + " >> " + td.getTimestamp());
		}

		// Send out a warning and reset to 0 for warnings.
		if (highMap.get(td.getSatelliteId()) != null && highMap.get(td.getSatelliteId()) == 3) {
			warnHigh(highFlagged.get(0));
			highFlagged.remove(0);
			highMap.put(td.getSatelliteId(), 0);
		} else if (lowMap.get(td.getSatelliteId()) != null && lowMap.get(td.getSatelliteId()) == 3) {
			warnLow(lowFlagged.get(0));
			lowFlagged.remove(0);
			lowMap.put(td.getSatelliteId(), 0);
		}
	}

	/**
	 * Gets last satellite that is of the same satellite ID.
	 * 
	 * @param td
	 *            TelemetryData object.
	 * @return TelemetryData object with same ID
	 */
	private TelemetryData getLastSatellite(TelemetryData td) {
		TelemetryData previousData = telemetryQueue.peek();
		for (TelemetryData previousTd : telemetryQueue) {
			if (td.getSatelliteId() == previousTd.getSatelliteId()
					&& td.getComponent() == previousData.getComponent()) {
				previousData = previousTd;
				break;
			}
		}

		// return td;
		return previousData;
	}

	/**
	 * Shows JSON output of first satellite occurrence that failed high constraints.
	 * 
	 * @param TelemetryData
	 *            object.
	 * @see #determineConstraints(TelemetryData)
	 */
	private void warnHigh(TelemetryData td) {
		PmcLogger.logEvent(Level.SEVERE,
				"Three consecutive high levels found for Satellite ID: " + td.getSatelliteId() + "\n\tRAW VALUE: "
						+ td.getRawValue() + "\n\tRED HIGH THRESHOLD: " + td.getRedHighLimit() + "\n\tDATE: "
						+ td.getTimestamp());

		String timestamp = td.getDateTime().toString().substring(0, td.getDateTime().toString().length() - 6) + "Z";
		String json = new GsonBuilder().setPrettyPrinting().create().toJson(new JsonData(td.getSatelliteId(),
				SeverityType.parse(SeverityType.RED_HIGH), Component.TSTAT, timestamp));
		System.out.println(json);
	}

	/**
	 * Shows JSON output of first satellite occurrence that failed low constraints.
	 * 
	 * @param TelemetryData
	 *            object.
	 * @see #determineConstraints(TelemetryData)
	 */
	private void warnLow(TelemetryData td) {
		PmcLogger.logEvent(Level.SEVERE,
				"Three consecutive low levels found for Satellite ID: " + td.getSatelliteId() + "\n\tRAW VALUE: "
						+ td.getRawValue() + "\n\tRED LOW THRESHOLD: " + td.getRedLowLimit() + "\n\tDATE: "
						+ td.getTimestamp());
		String timestamp = td.getDateTime().toString().substring(0, td.getDateTime().toString().length() - 6) + "Z";
		String json = new GsonBuilder().setPrettyPrinting().create().toJson(
				new JsonData(td.getSatelliteId(), SeverityType.parse(SeverityType.RED_LOW), Component.BATT, timestamp));
		System.out.println(json);
	}

}
