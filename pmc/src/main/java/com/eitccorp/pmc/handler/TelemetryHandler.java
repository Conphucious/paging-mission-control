package com.eitccorp.pmc.handler;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;

import org.apache.commons.io.FileUtils;

import com.eitccorp.pmc.PmcLogger;
import com.eitccorp.pmc.model.Component;
import com.eitccorp.pmc.model.TelemetryData;

/**
 * Handles the telemetry files. Acts as telemetry status data "engine"
 * 
 * @author Jimmy Nguyen
 * @since 11/06/2019
 *
 */

public class TelemetryHandler {

	private ConditionHandler warning;
	private List<Integer> satelliteIdList;
	private File file;

	public TelemetryHandler(String path) {
		warning = new ConditionHandler();

		file = new File(path);
		if (!file.exists())
			PmcLogger.logEvent(Level.SEVERE, "File does not exist");
	}

	private List<String> readTelemetryData() {
		PmcLogger.logEvent(Level.INFO, "Logging information from " + file.getAbsolutePath());
		List<String> lines = null;
		try {
			lines = FileUtils.readLines(file, "UTF-8");
		} catch (IOException e) {
			e.printStackTrace();
			PmcLogger.logEvent(Level.SEVERE, "Unable to read file from " + file.getAbsolutePath());
		}

		return lines;
	}

	private List<TelemetryData> parseStatusData() {
		List<TelemetryData> tmList = new ArrayList<>();
		satelliteIdList = new ArrayList<>();
		List<String> data = readTelemetryData();

		for (String status : data) {
			// new empty lines
			if (status.isEmpty())
				break;
			String[] parser = status.split("[\\|]");
			String timestamp = parser[0];

			int satelliteId = Integer.parseInt(parser[1]);
			satelliteIdList.add(satelliteId);

			int redHighLimit = Integer.parseInt(parser[2]);
			int yellowHighLimit = Integer.parseInt(parser[3]);
			int yellowLowLimit = Integer.parseInt(parser[4]);
			int redLowLimit = Integer.parseInt(parser[5]);
			double rawValue = Double.parseDouble(parser[6]);
			String component = parser[7];

			tmList.add(new TelemetryData(timestamp, satelliteId, redHighLimit, yellowHighLimit, yellowLowLimit,
					redLowLimit, rawValue, component));
		}

		PmcLogger.logEvent(Level.INFO, tmList.size() + " items have been parsed from " + file.getAbsoluteFile());
		return tmList;

	}

	public void checkStatus() {
		List<TelemetryData> tdList = parseStatusData();

		for (int i = 0; i < tdList.size(); i++) {
			warning.checkCondition(tdList.get(i));
		}
	}

}
