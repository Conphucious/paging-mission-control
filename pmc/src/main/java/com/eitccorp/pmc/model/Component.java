package com.eitccorp.pmc.model;

/**
 * Used to reference components.
 * 
 * @author Jimmy Nguyen
 * @since 11/06/2019
 *
 */

public enum Component {

	TSTAT, BATT;

	private Component() {}

	public static Component parse(String name) {
		if (name.equals("TSTAT"))
			return TSTAT;
		else if (name.equals("BATT"))
			return BATT;

		return null;
	}

}
