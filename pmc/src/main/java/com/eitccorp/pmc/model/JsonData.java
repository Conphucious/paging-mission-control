package com.eitccorp.pmc.model;

/**
 * Used as a template for the Json Output.
 * 
 * @author Jimmy Nguyen
 * @since 11/06/2019
 */
public class JsonData {

	private int satelliteId;
	private String severity;
	private Component component;
	private String timestamp;

	public JsonData(int satelliteId, String severity, Component component, String timestamp) {
		this.satelliteId = satelliteId;
		this.severity = severity;
		this.component = component;
		this.timestamp = timestamp;
	}

}
