package com.eitccorp.pmc.model;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;

/**
 * Model for TelemetryData to be parsed into.
 * 
 * @author Jimmy Nguyen
 * @since 11/06/2019
 *
 */

public class TelemetryData {

	private String timestamp;
	private DateTime dateTime;
	private int satelliteId;
	private int redHighLimit, yellowHighLimit, yellowLowLimit, redLowLimit;
	private double rawValue;
	private Component component;

	public TelemetryData(String timestamp, int satelliteId, int redHighLimit, int yellowHighLimit, int yellowLowLimit,
			int redLowLimit, double rawValue, String component) {
		this.timestamp = timestamp;
		dateTime = DateTime.parse(timestamp, DateTimeFormat.forPattern("yyyyMMdd HH:mm:ss.SSS"));
		this.satelliteId = satelliteId;
		this.redHighLimit = redHighLimit;
		this.yellowHighLimit = yellowHighLimit;
		this.yellowLowLimit = yellowLowLimit;
		this.redLowLimit = redLowLimit;
		this.rawValue = rawValue;
		this.component = Component.parse(component);
	}

	public String getTimestamp() {
		return timestamp;
	}

	public DateTime getDateTime() {
		return dateTime;
	}

	public int getSatelliteId() {
		return satelliteId;
	}

	public int getRedHighLimit() {
		return redHighLimit;
	}

	public int getYellowHighLimit() {
		return yellowHighLimit;
	}

	public int getYellowLowLimit() {
		return yellowLowLimit;
	}

	public int getRedLowLimit() {
		return redLowLimit;
	}

	public double getRawValue() {
		return rawValue;
	}

	public Component getComponent() {
		return component;
	}

}
