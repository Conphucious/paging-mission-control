package com.eitccorp.pmc.model;

/**
 * Used to reference SeverityType.
 * 
 * @author Jimmy Nguyen
 * @since 11/06/2019
 *
 */

public enum SeverityType {

	RED_HIGH, RED_LOW;

	private SeverityType() {}

	/**
	 * Parses SeverityType into a string
	 * 
	 * @param s
	 *            Severity Type
	 * @return String version of SeverityType
	 */
	public static String parse(SeverityType s) {
		if (s == RED_HIGH)
			return "RED HIGH";
		else if (s == RED_LOW)
			return "RED LOW";
		return null;
	}

}
